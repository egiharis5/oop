<?php

require_once('frog.php');
require_once('ape.php');
require_once('animal.php');

$object=new animal("shaun");

echo "Name : $object->name <br>"; // "shaun"
echo "legs : $object->legs <br>"; // 4
echo "Cold blooded : $object->cold_blooded <br>"; // "no"

$object2=new frog("buduk");

echo "<br> Name : $object2->name <br>"; // "shaun"
echo "Legs : $object2->legs <br>"; // 4
echo "Cold blooded : $object2->cold_blooded <br>"; // "no"
echo "Jump :";
$object2->jump();


$object3=new ape("sungokong");

echo "<br><br>Name : $object3->name <br>"; // "shaun"
echo "Legs : $object3->legs <br>"; // 4
echo "Cold blooded : $object3->cold_blooded <br>"; // "no"
echo "Yell : ";
$object3->yell();

